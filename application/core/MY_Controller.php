<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


class MY_Controller extends CI_Controller{
    /**
     * Carrega o código HTML das páginas (Exceto a Home)
     * @param conteudo: Código HTML  
     */
    public function show($conteudo){
        // incluir o cabeçalho
        $html = $this->load->view('common/header', null, true);
        $html .= $this->load->view('common/navbar', null, true);

        // incluir o conteúdo
        $html .= '<div class="container mt-5 pt-5 mb-5">';
        $html .= $conteudo;
        $html .= '</div>';

        // incluir o rodapé
        $html .= $this->load->view('common/rodape', null, true);
        $html .= $this->load->view('common/footer', null, true);
        echo $html;
    }

    /**
     * Carrega o código HTML da Homepage
     * @param fundoHome: Código HTML
     */
    public function showHome($fundoHome){
        // incluir o cabeçalho
        $html = '<header>';
        $html .= $this->load->view('common/header', null, true);
        $html .= $this->load->view('common/navbarHome', null, true);
        $html .= $fundoHome;
        $html .= '</header>';
        // incluir o rodapé
        $html .= $this->load->view('common/rodape', null, true);
        $html .= $this->load->view('common/footer', null, true);
        echo $html;
    }

}

