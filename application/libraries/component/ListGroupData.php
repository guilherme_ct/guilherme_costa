<?php
include_once 'Component.php';

class ListGroupData extends Component {
    /** Contém a quantidade de itens do list group */
    private $qtd_itens;

    /** Contém as classes do List Group */
    private $listgroup_classes;

    /** Contém as classes dos items do List Group */
    private $listgroup_item_classes;

    public function __construct($qtd_itens){
        $this->qtd_itens = $qtd_itens; 
    }
    
     /**
     * Informa as imagens do carrossel
     * @return qtd_itens: numero inteiro;
     */
    public function getQtd(){
        //Teste quantidade obrigatoria
        /*$qtd_itens=$this->qtd_itens;
        return $qtd_itens != '' ? true : $qtd_itens;*/

        //Teste quantidade deve ser inteiro
        /*$qtd_itens=$this->qtd_itens;
        return is_int($qtd_itens) ? true : $qtd_itens;*/

        //Teste quantidade deve ser no maximo 99
        $qtd_itens=$this->qtd_itens;
        return $qtd_itens <= 99 ? true : $qtd_itens;

    }

    /**
     * Informa a cor dos itens do list group
     * @return cor: string;
     */
    public function getColor(){
        //Teste cor deve ser valida
        $color=substr($this->listgroup_item_classes, 16);

        if($color == 'primary' || $color == 'secondary' || $color == 'success' || $color == 'danger' ||
           $color == 'warning' || $color == 'info' || $color == 'light' || $color == 'dark'){
            return true;
        }else{
            return $color;
        }
    }

    /** 
     * Gera o código HTML do carrossel
     * @return string: código html
     */ 
    public function getHTML(){
        $html = '<div class="card">
                    <div class="card-header">
                        Exemplo de List Group (MDBootstrap)
                    </div>
                    <div class="card-body">';
        $html.= $this->listGroup();
        $html.= '</div></div>';
        return $html;
    }

    /**
     * Gera o corpo do List Group
     * @return string: código html
     */
    public function listGroup(){
        $html='<ul class="list-group '.$this->listgroup_classes.'">';
        for($i = 1; $i <= $this->qtd_itens; $i++){
            $html .= '<li class="list-group-item '.$this->listgroup_item_classes.'">Item '.$i.'</li>';
        }
        $html.= '</ul>';
        return $html; 
    }

    /**
     * Torna os itens do List Group ativos na cor azul
     * @return string
     */
    public function useActive(){
        $this->listgroup_item_classes.= 'active ';
        return $this;
    }

    /**
     * Torna cinza o texto dos itens do List Group 
     * @return string
     */
    public function useDisabled(){
        $this->listgroup_item_classes.= 'disabled ';
        return $this;
    }

    /**
     * Remove algumas bordas e pontas arredondadas do List Group
     * @return string
     */
    public function useFlush(){
        $this->listgroup_classes.= 'list-group-flush ';
        return $this;
    }

    /**
     * Altera o layout do List Group de vertical para horizontal
     * @return string
     */
    public function useHorizontal(){
        $this->listgroup_classes.= 'list-group-horizontal ';
        return $this;
    }

    /**
     * Altera a cor dos itens do List Group
     * @param color: classe do bootstrap
     * @return string
     */
    public function useColor($color){
        $this->listgroup_item_classes.= "list-group-item-$color";
        return $this;
    }


    

    
}