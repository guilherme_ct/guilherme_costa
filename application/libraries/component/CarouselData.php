<?php
include_once 'Component.php';

class CarouselData extends Component {
    /** Contém as imagens do carrossel */
    private $images;

    /** Contém as classes do carrossel */
    private $carousel_classes = '';

    /** Contém os controles do carrossel */
    private $carousel_controls = '';

    /** Contém os indicadores de imagem carrossel */
    private $carousel_indicators = '';

    public function __construct($images){
        $this->images = $images; 
    }

    /**
     * Informa as imagens do carrossel
     * @return images: lista de strings separadas por um espaço;
     */
    public function getImages(){
        //Teste Minimo uma imagem
        /*$images=$this->images;
        return $images != '' ? true : $images;*/

        //Teste imagem deve ser string
        /*$images=$this->images;
        return is_string($images) ? true : $images;*/

        //Teste imagem deve ter extensão válida
        $images=$this->images;
        $ext = substr($images,-4);
        return $ext == '.jpg' || $ext == '.png' || $ext == '.jpeg' || $ext == '.gif' ? true : $images;
    }


    /** 
     * Gera o código HTML do carrossel
     * @return string: código html
     */ 
    public function getHTML(){
        $html = '<div class="card">
                    <div class="card-header">
                        Exemplo de Carousel (MDBootstrap)
                    </div>
                    <div class="card-body">';
        $html .= $this->header();
        $html .= $this->body();
        $html .= '</div>';
        $html .= $this->carousel_controls;
        $html .= '</div></div></div>';
        return $html;
    }

    /**
     * Gera o cabeçalho da carrosel
     * @return string: código html
     */
    private function header(){
        $html = '<div id="carousel-example-1z" class="carousel '.$this->carousel_classes.'" data-ride="carousel">';
        $html .= $this->carousel_indicators;
        $html .= '<div class="carousel-inner" role="listbox">';

        return $html;
    }

    /**
     * Gera o corpo do carrosel
     * @return string: código html
     */
    private function body(){
        $html = '<div class="carousel-item active">
                    <img class="d-block w-100" src="'.$this->images[0].'">
                 </div>'; 

        foreach(array_slice($this->images, 1) as $image){
            $html .= '<div class="carousel-item">
                        <img class="d-block w-100" src="'.$image.'">
                     </div>'; 
         }

        return $html;
    }

    /**
     * Adiciona controles no carrossel
     */
    

     /**
     * Insere classes no cabeçalho do carrossel
     * @param class: lista de strings separadas por espaço 
     * @return string;
     */
    public function addHeaderClass($class){
        $this->carousel_classes .= "$class ";
        return $this;
    }

    /**
     * Inclui o efeito de slide ao mudar de imagem no carrossel
     * @return string;
     */
    public function useSlide(){
        $this->carousel_classes .= 'slide ';
        return $this;
    }

    /**
     * Inclui o efeito de fade ao mudar de imagem no carrossel
     * @return string;
     */
    public function useFade(){
        $this->carousel_classes .= 'carousel-fade ';
        return $this;
    }

    /**
     * Adiciona botões de controle de imagem no carrossel  
     * @return string: código html
     */
    public function useControls(){
        $this->carousel_controls .= '<a class="carousel-control-prev" href="#carousel-example-1z" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Anterior</span>
                                    </a>';
        $this->carousel_controls .= '<a class="carousel-control-next" href="#carousel-example-1z" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Próximo</span>
                                    </a>';
        return $this;
    }

    /**
     * Adiciona indicadores de posição de imagem no carrossel
     * @return string: código html
     */
    public function useIndicators(){
        $this->carousel_indicators .= '<ol class="carousel-indicators">
                                           <li data-target="#carousel-example-1z" data-slide-to="0" class="active"></li>';
        for($i = 1; $i < sizeof($this->images); $i++){
            $this->carousel_indicators .= '<li data-target="#carousel-example-1z" data-slide-to="'.$i.'"></li>';
        }
        $this->carousel_indicators .= '</ol>';
        return $this;
    }



}