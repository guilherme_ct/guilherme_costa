<?php
include_once 'Component.php';

class Table extends Component {
    /** Contém os nomes das colunas da tabela */
    private $labels;

    /** Contém as classes do cabeçalho */
    private $header_classes = '';

    /** Contém as classes da tabela */
    private $table_classes = '';
    
    public function __construct($labels){
        $this->labels = $labels; 
    }

    /**
     * Informa as colunas da tabela
     * @return labels: lista de strings separadas por um espaço;
     */
    public function getLabels(){
        //Teste Primeira coluna obrigatoria
        /*$labels=$this->labels;
        return $labels!='' ? true : $labels;*/

        //Teste primeira coluna string
        /*
        $labels=$this->labels;
        if(is_string($labels)){
            return 'string';
        } else {
            return $labels;
        }*/

        $size=strlen($this->labels);
        return $size >= 2 ? true : $this->labels;

       
    }

    /** 
     * Gera o código HTML da tabela
     * @return string: código html
     */ 
    public function getHTML(){
        $html = '<div class="card">
                    <div class="card-header">
                        Exemplo de Sort Table (W3 Schools)
                    </div>
                    <div class="card-body">';              
        $html .= $this->header();
        $html .= $this->body();
        $html .= '</table> </div> </div>';
        return $html;
    }

    /**
     * Gera o cabeçalho da tabela
     * @return string
     */
    private function header(){
        $html = '<table id="sortTable" class="table '.$this->table_classes.'">';
        $html .= '<thead class="'.$this->header_classes.'"><tr>';
        $i=0;
        foreach ($this->labels as $rotulo) {
            $html .= '<th style="cursor:pointer;" onclick="sortTable('.$i.')">'.$rotulo.'<i class="fa fa-sort" style="float:right;"></i></th>';      
            $i++;      
        }
        $html .= '</tr></thead>';
        return $html;
    }

    /**
     * Gera o corpo da tabela
     * @return string
     */
    private function body(){
        $html = '';
        $html .= '<tr><td>Augusto</td><td>Ferreira</td><td>35</td></tr>';
        $html .= '<tr><td>Guilherme</td><td>Tavares</td><td>20</td></tr>';
        $html .= '<tr><td>Carlos</td><td>Alberto</td><td>58</td></tr>';
        $html .= '<tr><td>Douglas</td><td>Costa</td><td>41</td></tr>';
        $html .= '<tr><td>Bruno</td><td>Silva</td><td>23</td></tr>';
        return $html;
    }

    /**
     * Insere classes no cabeçalho da tabela
     * @param class: lista de strings separadas por espaço 
     */
    public function addHeaderClass($class){
        $this->header_classes .= "$class ";
        return $this;
    }

    /**
     * Exibe listras na tabela
     * @return string
     */
    public function useStripes(){
        $this->table_classes .= 'table-striped ';
        return $this;
    }

    /**
     * Exibe bordas na tabela
     * @return string
     */
    public function useBorder(){
        $this->table_classes .= 'table-bordered ';
        return $this;
    }

    /**
     * Inclui o efeito de mouseover nas linhas da tabela
     * @return string
     */
    public function useHover(){
        $this->table_classes .= 'table-hover ';
        return $this;
    }

    /**
     * Diminui a altura das linhas da tabela
     * @return string
     */
    public function smallRow(){
        $this->table_classes .= 'table-sm ';
        return $this;
    }

    /**
     * Corta pela metade o espaçamento interno da célula
     * @return string
     */
    public function smallCell(){
        $this->table_classes .= 'table-condensed ';
        return $this;
    }

    /**
     * Torna a tabela responsiva
     * @return string
     */
    public function respTable(){
        $this->table_classes .= 'table-responsive ';
        return $this;
    }

}