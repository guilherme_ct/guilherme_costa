<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH.'libraries/component/Table.php';

class SortTableModel extends CI_Model {
    /**
     * Chama as funções responsáveis por carregar a Sort Table
     * @return table: código HTML 
     */
    public function tabela(){
        $labels = array('Nome', 'Sobrenome', 'Idade');

        $table = new Table($labels);
        //Algumas funções podem ter sido comentadas para não dar conflito entre as classes 
        //e exibir o componente de forma coerente
        $table->addHeaderClass('purple-gradient white-text')
        ->useBorder()->useHover()->useStripes()->smallRow()->smallCell()/*->respTable()*/;
        return $table->getHTML();
    }

}