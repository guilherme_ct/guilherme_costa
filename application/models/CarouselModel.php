<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH.'libraries/component/CarouselData.php';

class CarouselModel extends CI_Model {
    /**
     * Chama as funções responsáveis por carregar o Carousel
     * @return carousel: código HTML 
     */
    public function carousel(){
        //Adicione quantas imagens quiser adicionando mais links de imagem
        /** Contem as imagens do Carousel */
        $images=array('https://mdbootstrap.com/img/Photos/Slides/img%20(35).jpg',
                      'https://mdbootstrap.com/img/Photos/Slides/img%20(33).jpg',
                      'https://mdbootstrap.com/img/Photos/Slides/img%20(31).jpg',
                      'https://mdbootstrap.com/img/Photos/Slides/img%20(45).jpg');

        $carousel = new CarouselData($images);
        $carousel->useSlide()->useFade()->useControls()->useIndicators();
       
        return $carousel->getHTML();
    }
}