<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH.'/controllers/test/MyToast.php');
include_once APPPATH.'libraries/component/ListGroupData.php';

class ListGroupTest extends MYToast{

    function __construct(){
        parent::__construct('ListGroupTest');
    }

    // Caso de teste 1
    /**
     * Testa se o parâmetro da função ListGroupData() foi preenchido com algum valor
     */
    function test_qtd_obrigatoria(){
        $listgroup = new ListGroupData(3);
        $actual = $listgroup->getQtd();
        $this->_assert_equals_strict(true, $actual, "A quantidade é obrigatoria");
    }

    // Caso de teste 2
    /**
     * Testa se o parâmetro da função ListGroupData() é um valor inteiro
     */
    function test_qtd_eh_inteiro(){
        $listgroup = new ListGroupData(1);
        $actual = $listgroup->getQtd();
        $this->_assert_equals_strict(true, $actual, "Erro: A quantidade deve ser um número inteiro. Recebido($actual)");
    }

    // Caso de teste 3
    /**
     * Testa se o parâmetro da função ListGroupData() tem no máximo 2 caracteres
     */
    function test_qtd_maximo_2_chars(){
        $listgroup = new ListGroupData(99);
        $actual = $listgroup->getQtd();
        $this->_assert_equals_strict(true, $actual, "Erro: A quantidade deve ser no maximo 99. Recebido($actual)");
    }

    // Caso de teste 4
    /**
     * Testa se o parâmetro da função ListGroupData() é uma cor válida do bootstrap
     */
    function test_cor_valida(){
        $listgroup = new ListGroupData(10);
        $listgroup->useColor('success');
        $actual = $listgroup->getColor();
        $this->_assert_equals_strict(true, $actual, "Erro: As cores válidas são (primary, secondary, success, danger, warning, info, light, dark). Recebido($actual)");
    }


    

}