<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH.'/controllers/test/MyToast.php');
include_once APPPATH.'libraries/component/Table.php';

class SortTableTest extends MYToast{

    function __construct(){
        parent::__construct('SortTableTest');
    }

    // Caso de teste 1
    /**
     * Testa se a função Table() recebeu ao menos um parâmetro
     */
    function test_primeira_coluna_obrigatoria(){
        $table = new Table('Coluna 1','');
        $actual = $table->getLabels();
        $this->_assert_equals(true, $actual, "Pelo menos uma coluna deve ser preenchida");
    }

    // Caso de teste 2
    /**
     * Testa se o parâmetro da função Table() é uma string
     */
    function test_primeira_coluna_string(){
        $table = new Table('Coluna','');
        $actual = $table->getLabels();
        $this->_assert_equals('string', $actual, "Erro: esperado(string), recebido ($actual)");
    }

    // Caso de teste 3
    /**
     * Testa se o parâmetro da função Table() possui ao menos 2 caracteres
     */
    function test_minimo_2_caracteres(){
        $table = new Table('Coluna1');
        $actual = $table->getLabels();
        $this->_assert_equals_strict(true, $actual, "O nome deve ter, pelo menos, 2 chars");
    }

    




}