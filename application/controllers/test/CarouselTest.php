<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH.'/controllers/test/MyToast.php');
include_once APPPATH.'libraries/component/CarouselData.php';

class CarouselTest extends MYToast{

    function __construct(){
        parent::__construct('CarouselTest');
    }

    // Caso de teste 1
    /**
     * Testa se a função CarouselData() recebeu pelo menos um parametro
     */
    function test_minimo_uma_imagem(){
        $carousel = new CarouselData('imagem.jpg');
        $actual = $carousel->getImages();
        $this->_assert_equals_strict(true, $actual, "Pelo menos uma imagem é obrigatória");
    }

    // Caso de teste 2
    /**
     * Testa se o parâmetro da função CarouselData() é uma string
     */
    function test_imagem_deve_ser_string(){
        $carousel = new CarouselData('imagem.jpg');
        $actual = $carousel->getImages();
        $this->_assert_equals_strict(true, $actual, "A imagem deve ser uma string");
    }

    // Caso de teste 3
    /**
     * Testa se o parâmetro da função CarouselData() possui uma extensão válida de imagem
     */
    function test_extensao_valida(){
        $carousel = new CarouselData('imagem.png');
        $actual = $carousel->getImages();
        $this->_assert_equals_strict(true, $actual, "A imagem deve ter uma extensão válida (.jpg, .png, .jpeg, .gif)");
    }

    

}