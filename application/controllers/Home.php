<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

	 /**
     * Carrega a página HTML da Home
     */
	public function index()
	{
            $fundo_home = $this->load->view('home', false, true);
            $this->showHome($fundo_home);
	}

}