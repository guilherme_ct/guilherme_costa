<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SortTable extends MY_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('SortTableModel', 'sorttable');
    }
     /**
     * Carrega a página HTML do Sort Table
     */
	public function index() {
        $data['tabela'] = $this->sorttable->tabela();
        $html = $this->load->view('sort_table/tabela', $data, true);
        $this->show($html);
    }
}
