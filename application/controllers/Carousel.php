<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Carousel extends MY_Controller{
    
    function __construct(){
        parent::__construct();
        $this->load->model('CarouselModel', 'carousel');
    }
    /**
     * Carrega a página HTML do Carousel
     */
	public function index() {
        $data['carousel'] = $this->carousel->carousel();
        $html = $this->load->view('carousel/carrossel', $data, true);
        $this->show($html,'');
    }
}