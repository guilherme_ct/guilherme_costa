<link href="<?= base_url('assets/mdb/css/style.css') ?>" rel="stylesheet">
<!-- Main navigation -->
<header>
  <!-- Navbar -->
  <nav class="navbar navbar-expand-lg navbar-light fixed-top scrolling-navbar">
    <div class="container">
      <a class="navbar-brand" href="<?php echo base_url() ?>"><strong>LP2</strong></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-7"
        aria-controls="navbarSupportedContent-7" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent-7">
        <!-- Left -->
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link waves-effect" href="<?php echo base_url() ?>">Início
            </a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown"
              aria-haspopup="true" aria-expanded="false">Componentes</a>
            <div class="dropdown-menu dropdown-primary" aria-labelledby="navbarDropdownMenuLink">
              <a class="dropdown-item" href="<?php echo base_url('sortTable') ?>">Sort Table</a>
              <a class="dropdown-item" href="<?php echo base_url('carousel') ?>">Carousel</a>
              <a class="dropdown-item" href="<?php echo base_url('listGroup') ?>">List Group</a>
            </div>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown"
              aria-haspopup="true" aria-expanded="false">Testes Unitários</a>
            <div class="dropdown-menu dropdown-primary" aria-labelledby="navbarDropdownMenuLink">
              <a class="dropdown-item" href="<?php echo base_url('test/sortTableTest') ?>">Sort Table</a>
              <a class="dropdown-item" href="<?php echo base_url('test/carouselTest') ?>">Carousel</a>
              <a class="dropdown-item" href="<?php echo base_url('test/listGroupTest') ?>">List Group</a>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link waves-effect" href="<?php echo base_url('docs/api/index.html') ?>" target="_blank">Documentação</a>
          </li>
        </ul>
        
      </div>
    </div>
  </nav>
  <!-- Navbar -->
  