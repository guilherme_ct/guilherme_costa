<!-- Footer -->
<footer class="page-footer font-small bg-dark pt-4">

  <!-- Footer Links -->
  <div class="container-fluid text-center text-md-left">

    <!-- Grid row -->
    <div class="row">

      <!-- Grid column -->
      <div class="col-md-6 mt-md-0 mt-3">

        <!-- Content -->
        <h5 class="text-uppercase">Linguagem de programação 2</h5>
        <p>Modelando Componentes com PHP orientado a objetos</p>

      </div>
      <!-- Grid column -->

      <hr class="clearfix w-100 d-md-none pb-3">

      <!-- Grid column -->
      <div class="col-md-3 mb-md-0 mb-3">

        <!-- Links -->
        <h5 class="text-uppercase">Links</h5>

        <ul class="list-unstyled">
          <li>
            <a href="<?php echo base_url('') ?>">Início</a>
          </li>
          <li>
            <a href="<?php echo base_url('sortTable') ?>">Sort Table</a>
          </li>
          <li>
            <a href="<?php echo base_url('carousel') ?>">Carousel</a>
          </li>
          <li>
            <a href="<?php echo base_url('listGroup') ?>">List Group</a>
          </li>
        </ul>

      </div>
      <!-- Grid column -->

    </div>
    <!-- Grid row -->

  </div>
  <!-- Footer Links -->

  <!-- Copyright -->
  <div class="footer-copyright text-center py-3">© IFSP - 2019 Modelando Componentes com PHP orientado a objetos
  </div>
  <!-- Copyright -->

</footer>
<!-- Footer -->