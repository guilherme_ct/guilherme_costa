
<!-- Full Page Intro -->
<div class="view  jarallax" data-jarallax='{"speed": 0.2}' style="background-image: url('https://mdbootstrap.com/img/Photos/Others/gradient2.png'); background-repeat: no-repeat; background-size: cover; background-position: center center;">
    <!-- Mask & flexbox options-->
    <div class="mask rgba-purple-slight d-flex justify-content-center align-items-center">
      <!-- Content -->
      <div class="container">
        <!--Grid row-->
        <div class="row wow fadeIn">
          <!--Grid column-->
          <div class="col-md-12 text-center">
            <h1 class="display-4 font-weight-bold mb-0 pt-md-5 pt-5 wow fadeInUp">AT02 - Modelando Componentes com PHP orientado a objetos</h1>
            <h5 class="pt-md-5 pt-sm-2 pt-5 pb-md-5 pb-sm-3 pb-5 wow fadeInUp" data-wow-delay="0.2s"></h5>
            <div class="wow fadeInUp" data-wow-delay="0.4s">
            
              <a class="btn btn-purple btn-rounded dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="fas fa-cogs left"></i> Componentes</a>
              <div class="dropdown-menu">
                <a class="dropdown-item" href="<?php echo base_url('sortTable') ?>">Sort Table</a>
                <a class="dropdown-item" href="<?php echo base_url('carousel') ?>">Carousel</a>
                <a class="dropdown-item" href="<?php echo base_url('listGroup') ?>">List Group</a>
              </div>

              <a class="btn btn-purple btn-rounded" href="<?php echo base_url('docs/api/index.html') ?>" target="_blank"><i class="fas fa-book left"></i> Documentação</a>
            
            </div>
          </div>
          <!--Grid column-->
        </div>
        <!--Grid row-->
      </div>
      <!-- Content -->
    </div>
    <!-- Mask & flexbox options-->
  </div>
  <!-- Full Page Intro -->
</header>
<!-- Main navigation -->
