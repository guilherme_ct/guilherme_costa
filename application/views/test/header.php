<?php $this->load->view('common/header'); ?>
<style type="text/css">
* { font-family: Arial, sans-serif; font-size: 9pt }
#results { width: 100% }
.err, .pas { color: white; font-weight: bold; margin: 2px 0; padding: 5px; vertical-align: top; }
.err { background-color: red }
.pas { background-color: #6EA6F4 }
.detail { padding: 8px 0 8px 20px }
h1 { font-size: 12pt }
a:link, a:visited { text-decoration: none; color: white }
a:active, a:hover { text-decoration: none; color: black; background-color: yellow }
</style>

<div class="p-4">
<a href="<?php echo base_url('')?>">
    <button class="btn btn-lg btn-dark"> <i class="fa fa-arrow-left"> </i> Voltar</button>
</a>
<hr>
<div class="card mt-4">
<div class="card-header">
    Toast - CodeIgniter Unit Test Plugin for LPII
</div>
<div class="card-body">

<br>
<ol>
