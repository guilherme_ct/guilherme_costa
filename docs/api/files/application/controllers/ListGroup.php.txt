<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ListGroup extends MY_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('ListGroupModel', 'listgroup');
    }
     /**
     * Carrega a página HTML do List Group
     */
	public function index() {
        $data['lista'] = $this->listgroup->listgroup();
        $html = $this->load->view('list_group/lista', $data, true);
        $this->show($html);
    }
}
