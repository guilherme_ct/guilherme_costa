<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH.'libraries/component/ListGroupData.php';

class ListGroupModel extends CI_Model {
    /**
     * Chama as funções responsáveis por carregar o List Group
     * @return listgroup: código HTML 
     */
    public function listgroup(){
        $listgroup = new ListGroupData(5);

        //Algumas funções podem ter sido comentadas para não dar conflito entre as classes e 
        //exibir o componente de forma coerente
        $listgroup->useFlush()->useColor('danger')/*->useActive()->useDisabled()->useHorizontal()*/;
       
        return $listgroup->getHTML();
    }
}
